import React from "react";

import FlipMove from "react-flip-move";

import './task.css';

const Task = (props) => {
    return (
        <div className="task">
            {
            props.tasks.map((task) => {
                return <FlipMove  duration={350} easing="ease-out">
                    <p key={task.id}>
                        {task.title}
                        <span className="action-btn" role="img" aria-label="delete" onClick={props.remove}>❌</span>
                    </p>
                 </FlipMove>
            })
        }
        </div>
    );
};

export default Task;