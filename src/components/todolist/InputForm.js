import React from 'react';

import './InputForm.css';

const InputForm = (props) => {
    return (
        <div className="todoList">
            <form>
                <input onChange={props.change} type="text" placeholder="Enter task"/>
                <input onClick={props.onSubmit} className="button" value="Add" type="submit"/>
            </form>
        </div>
    );
};

export default InputForm;

