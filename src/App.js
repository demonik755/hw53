import React, {Component} from 'react';

import './App.css';
import InputForm from "./components/todolist/InputForm";
import Task from "./components/tasks/Tasks";


class App extends Component {
  state = {
      title: " ",
    tasks: [
      {id: 0, title: 'Create todo list', done: false},
      {id: 1, title: 'Load on Bitbucket', done: false},
      {id: 2, title: 'Go to sleep ', done: false}
    ]
  };
    onChange = (event) => {
        const title = event.target.value;
        this.setState({title})
    };

    onSubmit = (event) => {
        event.preventDefault();
        const task = {id: new Date(), title: this.state.title}
        this.setState({tasks: [...this.state.tasks, task]
        });
    };
    deleteTask = (index) => {
      const tasks = [...this.state.tasks];
        tasks.splice(index, 1);
      this.setState({tasks})
    };
  render() {
    return (
        <div className="App">
            <h1 className="header">Tasks List</h1>
            <InputForm onSubmit={this.onSubmit} change={event => this.onChange(event)}/>
            <Task
                tasks={this.state.tasks}
                remove={(index) => this.deleteTask(index)}
            />
        </div>
    );
  }
}



export default App;
